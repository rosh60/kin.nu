$(document).ready(function(){
   $('#shorten').on('submit', function(){
       $('.success').fadeOut();
       var data = $(this).serialize();
       $.ajax({
           type: 'POST',
           url: 'home/submit',
           data: data,
           success: function(response){
               $('.success').text(response).slideDown();
           }
       });
       return false;
   });
});