<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
        $this->load->view('home/header', array('css' => 'public/css/home.css'));
        $this->load->view('home/index');
        $this->load->view('home/footer');
	}

    public function submit(){
        if(isset($_POST['longurl'])){
            $this->load->model('home_model');
            $longurl = $_POST['longurl'];
            $custurl = null;
            if(!empty($_POST['custurl'])){
                if(!ctype_alnum($_POST['custurl'])){
                    echo 'Only alphanumeric characters are allowed as custom URL.';
                    exit;
                }else{
                    $custurl = $_POST['custurl'];
                }
            }else{
                $this->load->helper('string');
                $custurl = random_string('alnum', 3);
                while($this->home_model->checkShortUrlExists($custurl)){
                    $custurl = random_string('alnum', 3);
                }
            }
            if(!filter_var($longurl, FILTER_VALIDATE_URL)){
                echo 'Invalid URL. Don\'t forget to include http://.';
                exit;
            }
            $user = $this->session->userdata('logged_in') == true ? $this->home_model->getLoggedUserName() : 'guest';
            $this->home_model->insert($longurl, $custurl, $user);
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        header('Location: '.base_url('login'));
    }

}
