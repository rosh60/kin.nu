<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') == true) header('Location: '.base_url('home'));
    }

    public function index()
    {
        $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
        $this->load->view('dashboard/register');
        $this->load->view('dashboard/footer');
    }

    public function submit(){
        $this->load->model('register_model');
        if($this->register_model->validateForm() == false){
            $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
            $this->load->view('dashboard/register', array('error' => validation_errors()));
            $this->load->view('dashboard/footer');
        }else{
            $val =  $this->db->query("INSERT INTO users (email, name, password) VALUES
            ('". $_POST['email'] ."', '". $_POST['fullname'] ."', '". sha1($_POST['password']) ."')");
            if($val == true){
                $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
                $this->load->view('dashboard/register', array(
                    'error' => '<div class="alert alert-success">Registration successful! You may login now.</div>')
                );
                $this->load->view('dashboard/footer');
            }
        }
    }

}
