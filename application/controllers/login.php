<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') == true) header('Location: '.base_url('home'));
    }

    public function index()
    {
        $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
        $this->load->view('dashboard/login');
        $this->load->view('dashboard/footer');
    }

    public function verify(){
        $this->load->model('login_model');
        if($this->login_model->validateLoginForm()){
            if($this->login_model->verifyLogin()){
                header('Location: '.base_url('home'));
            }else{
                $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
                $this->load->view('dashboard/login', array(
                        'error' => '<div class="alert alert-error">Invalid username or password.</div>')
                );
                $this->load->view('dashboard/footer');
            }
        }else{
            $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
            $this->load->view('dashboard/login', array('error' => validation_errors()));
            $this->load->view('dashboard/footer');
        }
    }

}
