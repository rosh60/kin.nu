<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 26/6/13
 * Time: 7:36 PM
 */

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if($this->session->userdata('logged_in') == false) header('Location: '.base_url('login'));
    }

    public function index(){
        $this->load->model('dashboard_model');
        $query = $this->dashboard_model->getUrlList();
        $this->load->view('dashboard/header', array('css' => 'public/css/dashboard.css'));
        $this->load->view('dashboard/dashboard', array('result' => $query));
        $this->load->view('dashboard/footer');
    }

    public function profile(){
        $this->load->model('dashboard_model');
        $query = $this->dashboard_model->getProfileInfo();
        if(isset($_POST['fullname'])){
            if($this->dashboard_model->validateProfileForm()){
                if($this->dashboard_model->checkCurrentPassword()){
                    $this->dashboard_model->updateUserProfile();
                    $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
                    $this->load->view('dashboard/profile', array('query' => $query, 'error' => '<div class="alert alert-success">Updated Successfully.</div>'));
                    $this->load->view('dashboard/footer');
                }else{
                    $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
                    $this->load->view('dashboard/profile', array('query' => $query, 'error' => '<div class="alert alert-error">Invalid current password.</div>'));
                    $this->load->view('dashboard/footer');
                }
            }else{
                $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
                $this->load->view('dashboard/profile', array('query' => $query, 'error' => validation_errors()));
                $this->load->view('dashboard/footer');
            }
        }else{
            $this->load->view('dashboard/header', array('css' => 'public/css/login.css'));
            $this->load->view('dashboard/profile', array('query' => $query));
            $this->load->view('dashboard/footer');
        }
    }

}