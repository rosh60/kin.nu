<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redirect extends CI_Controller {

    public function index()
    {
        $url = $this->uri->segment(1);
        if(ctype_alnum($url)){
            $this->load->model('redirect_model');
            $longurl = $this->redirect_model->getLongUrl($url);
            if(!$longurl == 0){
                header('Location: '. $longurl);
            }else{
                $this->load->view('home/linkdead');
            }
        }else{
            echo 'Invalid URL.';
        }
    }
}
