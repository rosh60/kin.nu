<html>
<head>
    <title>Kin.Nu - Shorten Your URLs.</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?php echo base_url($css); ?>" />
</head>
<body>
<header>
    <div id="container">
        <header>
            <nav>
                <ul>
                    <?php if($this->session->userdata('logged_in') == false): ?>
                    <li><a href="<?php echo base_url('login'); ?>">Login</a></li>
                    <li><a href="<?php echo base_url('register'); ?>">Register</a></li>
                    <?php else: ?>
                    <li><a href="<?php echo base_url('home/logout'); ?>">Logout</a></li>
                    <li><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
                    <?php endif; ?>
                </ul>
            </nav>
            <div class="clearfix"></div>
            <img id="logo" src="<?php echo base_url(); ?>public/images/logo.png" />
        </header>
