<div id="content">
    <h1>URL Shortener service to shorten your long URLs!</h1>
    <form id="shorten">
        <input  type="text" id="longurl" name="longurl" />
        <input type="submit" value="Shorten" id="btnShorten" /><br>
        <label for="custurl">Custom URL (Optional) : </label><input type="text" id="custurl" name="custurl" />
    </form>
</div>
<div class="success">Successful operation message</div>
<div class="error">Error message</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/home.js"></script>