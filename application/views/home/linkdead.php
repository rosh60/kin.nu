<!DOCTYPE html>
<html>
<head>
    <title>Kin.nu - URL Shortner</title>
    <style>
        .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
    </style>
    <link href="<?php echo base_url('public/css/bootstrap.css')?>" rel="stylesheet">
</head>
<body>
<div class="hero-unit center">
    <h1>Link Not Found <small><font face="Tahoma" color="red">Error 404</font></small></h1>
    <br />
    <p>Oops, Kin.nu couldn't find a link for the short URL you clicked.</p>
    <p><b>Most kin.nu URLs are 3-5 characters, and only include alphanumerics (case sensitive).</b></p>
    <a href="<?php echo base_url();?>" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Take Me Home</a>
</div>
</body>
</html>