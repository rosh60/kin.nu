<!DOCTYPE html>
<html>
<head>
    <title>Kin.nu - Login</title>
    <link href="<?php echo base_url('public/css/bootstrap.css')?>" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="<?php echo base_url($css)?>" />
</head>
<body>
<div class="span12" id="wrap" >
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <a class="brand" href="<?php echo base_url()?>"><img src="<?php echo base_url('public/images/kinnu.png')?>" width="80px" /></a>
            <ul class="nav">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <?php if($this->session->userdata('logged_in') == true):?>
                    <li><a href="<?php echo base_url('dashboard')?>">Dashboard</a></li>
                    <li><a href="<?php echo base_url('dashboard/profile')?>">Profile</a></li>
                    <li><a href="<?php echo base_url('home/logout')?>">Logout</a></li>
                <?php else: ?>
                    <li><a href="<?php echo base_url('dashboard')?>">Dashboard</a></li>
                    <li><a href="<?php echo base_url('register')?>">Register</a></li>
                    <li><a href="<?php echo base_url('login')?>">Login</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>