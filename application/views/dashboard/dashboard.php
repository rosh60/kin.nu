<div id="logo"><img src="<?=base_url('public/images/kinnu.png')?>" /></div>
<div class="input-append">
    <form action="#" id="shorten" method="POST">
        <input class="span8" id="appendedInputButton" type="text" name="longurl">
        <button class="btn">Shorten Now!</button><br><br>
        <div class="input-prepend">
            <span class="add-on">Custom URL (Optional) : </span>
            <input class="span3" id="prependedInput" type="text" name="custurl" />
        </div>
    </form>
    <div class="success">Successful operation message</div>
</div>
<table class="table table-bordered">
    <tr class="info">
        <td><strong>ID</strong></td>
        <td><strong>Short URL</strong></td>
        <td><strong>Long URL</strong></td>
        <td><strong>Visits</strong></td>
    </tr>
    <?php foreach($result->result() as $row): ?>
    <tr>
        <td><?=$row->id?></td>
        <td><?=base_url().$row->short?></td>
        <td><?=(strlen($row->longurl) > 77) ? substr($row->longurl, 0, 77).'...' : $row->longurl?></td>
        <td>0</td>
    </tr>
    <?php endforeach; ?>
</table>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?=base_url('public/js/dashboard.js')?>"></script>