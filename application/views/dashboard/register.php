<div id="register">
        <form class="form-horizontal" action='<?php echo base_url('register/submit')?>' method="POST">
            <fieldset>
                <div id="legend">
                    <legend class="">Register</legend>
                    <?php if(isset($error)) echo $error; ?>
                </div>
                <div class="control-group">
                    <!-- Username -->
                    <label class="control-label" for="username">Full Name</label>
                    <div class="controls">
                        <input type="text" id="username" name="fullname" placeholder="" class="input-xlarge">
                        <p class="help-block">Full name can contain any letters or numbers.</p>
                    </div>
                </div>

                <div class="control-group">
                    <!-- E-mail -->
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input type="email" id="email" name="email" placeholder="" class="input-xlarge">
                        <p class="help-block">Please provide your E-mail</p>
                    </div>
                </div>

                <div class="control-group">
                    <!-- Password-->
                    <label class="control-label" for="password">Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
                        <p class="help-block">Password should be at least 4 characters</p>
                    </div>
                </div>

                <div class="control-group">
                    <!-- Password -->
                    <label class="control-label" for="password_confirm">Password (Confirm)</label>
                    <div class="controls">
                        <input type="password" id="password_confirm" name="cpassword" placeholder="" class="input-xlarge">
                        <p class="help-block">Please confirm password</p>
                    </div>
                </div>

                <div class="control-group">
                    <!-- Button -->
                    <div class="controls">
                        <button class="btn btn-primary btn-large">Register</button>
                    </div>
                </div>
            </fieldset>
        </form>
</div>
