<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 22/6/13
 * Time: 8:59 PM
 */

class Home_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function insert($longurl, $custurl, $user){
        if($this->checkShortUrlExists($custurl)){
            echo 'Selected custom URL already exists!';
        }else{
            $sql = "INSERT INTO links VALUES('', '$longurl', '$custurl', '$user', '')";
            $flag = $this->db->query($sql);
            if($flag){
                echo site_url().$custurl;
            }else{
                echo 'An error occurred! Please try again.';
            }
        }
    }

    function checkShortUrlExists($short){
        $sql = "SELECT id FROM links WHERE short = '". $short . "'";
        $query = $this->db->query($sql);
        return ($query->num_rows() >= 1) ? true : false;
    }

    function getLoggedUserName(){
        $token = $this->session->userdata('token');
        $query = $this->db->query("SELECT email FROM users WHERE token = '{$token}'");
        if($query->num_rows() > 0){
            return $query->row('email');
        }else{
            echo 'Please log in.';
            exit;
        }
    }

}