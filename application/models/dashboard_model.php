<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 22/6/13
 * Time: 8:59 PM
 */

class Dashboard_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function getUrlList(){
        $user = $this->getLoggedUserName();
        $query = $this->db->query("SELECT * FROM links WHERE user = '{$user}'");
        return $query;
    }

    public function getLoggedUserName(){
        $token = $this->session->userdata('token');
        $query = $this->db->query("SELECT email FROM users WHERE token = '{$token}'");
        return $query->row('email');
    }

    public function getProfileInfo(){
        $token = $this->session->userdata('token');
        $query = $this->db->query("SELECT * FROM users WHERE token = '{$token}'");
        return $query->row();
    }

    public function validateProfileForm(){
        $this->load->library('form_validation');
        $rules = array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'required|min_length[3]|xss_clean'
            ),
            array(
                'field' => 'email',
                'label' => 'E-mail',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'cpassword',
                'label' => 'Current Password',
                'rules' => 'required'
            ),
            array(
                'field' => 'npassword',
                'label' => 'New Password',
                'rules' => "required|min_length[4]"
            ),
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
        return $this->form_validation->run();
    }

    public function checkCurrentPassword(){
        $query = $this->db->query("SELECT password FROM users WHERE token = '{$this->session->userdata('token')}'")->row();
        return ($query->password === sha1($_POST['cpassword']));
    }

    public function updateUserProfile(){
        $password = sha1($_POST['npassword']);
        return $this->db->query("UPDATE users SET email = '{$_POST['email']}', name = '{$_POST['fullname']}', password = '{$password}' WHERE token = '{$this->session->userdata('token')}'");
    }

}