<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 22/6/13
 * Time: 8:59 PM
 */

class Redirect_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function getLongUrl($short){
        $sql = "SELECT longurl FROM links WHERE short = '" . $short ."'";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            $this->hitCounter($short);
            return $query->row('longurl');
        }else{
            return 0;
        }
    }

    public function hitCounter($short)
    {
        $sql = "UPDATE links SET hits = hits + 1 WHERE short = '{$short}'";
        $this->db->query($sql);
    }
}