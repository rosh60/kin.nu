<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 22/6/13
 * Time: 8:59 PM
 */

class Login_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function validateLoginForm(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
        return $this->form_validation->run();
    }

    public function verifyLogin(){
        $password = sha1($this->input->post('password'));
        $query = $this->db->query("SELECT id FROM users WHERE email = '{$this->input->post('email')}'
            AND password = '{$password}'");
        if($query->num_rows()>0){
            $token = md5(random_string('unique'));
            $this->session->set_userdata(array('logged_in' => true, 'token' => $token));
            $id = $query->row('id');
            $this->db->query("UPDATE users SET token = '{$token}' WHERE id = {$id}");
            return true;
        }else{
            return false;
        }
    }

}