<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Roshan
 * Date: 22/6/13
 * Time: 8:59 PM
 */

class Register_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function validateForm(){
        $this->load->library('form_validation');
        $rules = array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'required|min_length[3]|xss_clean'
            ),
            array(
                'field' => 'email',
                'label' => 'E-mail',
                'rules' => 'required|valid_email|is_unique[users.email]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[4]'
            ),
            array(
                'field' => 'cpassword',
                'label' => 'Confirm Password',
                'rules' => "required|matches[password]"
            ),
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('matches', 'Passwords did not match.');
        $this->form_validation->set_message('is_unique', 'E-mail already exists in database.');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
        return $this->form_validation->run();
    }

}